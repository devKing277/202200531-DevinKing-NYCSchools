//
//  UIView+.swift
//  nycSchools
//
//  Created by  on 5/26/22.
//

import UIKit

// MARK: - Pin View

extension UIView {
    
    /// Pins a view to all perimeter anchors of a given view. This function also configures the auto translate flag.
    /// - Parameter view: The view to pin to.
    func pinTo(view: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        leadingAnchor.constraint(equalTo:  view.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo:  view.trailingAnchor).isActive = true
    }
    
}

// MARK: - Proportional Size

extension UIView {
    
    
    /// Sets proportional size constraints to a given view. This function also configures the auto translate flag.
    func proportionalSize(width: NSLayoutDimension? , widthPercent: CGFloat,
                          height: NSLayoutDimension? , heightPercent: CGFloat) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if  let width = width {
            widthAnchor.constraint(equalTo: width , multiplier: widthPercent).isActive = true
        }
        
        if  let height = height {
            heightAnchor.constraint(equalTo: height , multiplier: heightPercent).isActive = true
            
        }
    }
}


// MARK: - Center View

extension UIView {
    
    /// Sets the center axis anchors to a given view's axis anchors. This function also configures the auto translate flag.
    func center(centerX: NSLayoutXAxisAnchor?, paddingX: CGFloat?,
                centerY: NSLayoutYAxisAnchor?, paddingY: CGFloat?)  {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let centerX = centerX {
            centerXAnchor.constraint(equalTo: centerX , constant: paddingX ?? 0).isActive = true
        }
        
        if let centerY = centerY {
            centerYAnchor.constraint(equalTo: centerY , constant: paddingY ?? 0).isActive = true
        }
    }
}


// MARK: - Set Size

extension UIView {
    
    /// Sets the size of a given view. This function also configures the auto translate flag.
    func setSize(width: CGFloat , height: CGFloat)  {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if  width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if  height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
}


// MARK: - Set Anchors

extension UIView {
    
    /// Sets the anchors of a view to the anchors of a another view. This function also configures the auto translate flag.
    func anchor(top : NSLayoutYAxisAnchor?, paddingTop : CGFloat,
                bottom : NSLayoutYAxisAnchor?, paddingBottom : CGFloat,
                left: NSLayoutXAxisAnchor?, paddingLeft: CGFloat,
                right: NSLayoutXAxisAnchor?, paddingRight: CGFloat,
                width: CGFloat?,
                height: CGFloat?) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: paddingBottom).isActive = true
        }
        
        if let left = left {
            leadingAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        
        if let right = right {
            trailingAnchor.constraint(equalTo: right, constant: paddingRight).isActive = true
        }
        
        if let width = width, width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if let height = height, height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
            
        }
    }
}
