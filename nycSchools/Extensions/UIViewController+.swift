//
//  UIViewController+.swift
//  nycSchools
//
//  Created by  on 5/29/22.
//

import Foundation
import SwiftUI

extension UIViewController {
    
    /// Add a SwiftUI ``View`` as a child of the input ``UIView``.
    /// - Parameters:
    ///   - swiftUIView: The SwiftUI ``View`` to add as a child.
    ///   - view: The ``UIView`` instance to which the view should be added.
    func addSubSwiftUIView<Content>(_ swiftUIView: Content, to view: UIView) where Content : View {
        
        let hostingController = UIHostingController(rootView: swiftUIView)
        
        /// Add as a child of the current view controller.
        addChild(hostingController)
        
        /// Add the SwiftUI view to the view controller view hierarchy.
        view.addSubview(hostingController.view)
        
        /// Setup the contraints to update the SwiftUI view boundaries.
        hostingController.view.pinTo(view: view)
        
        /// Notify the hosting controller that it has been moved to the current view controller.
        hostingController.didMove(toParent: self)
    }
}
