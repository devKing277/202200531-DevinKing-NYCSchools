//
//  View+.swift
//  nycSchools
//
//  Created by  on 5/29/22.
//

import Foundation
import SwiftUI

extension View {
    /// Sets the corner radius of all given corners.
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}
