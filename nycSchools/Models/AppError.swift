//
//  AppError.swift
//  nycSchools
//
//  Created by  on 5/26/22.
//

import Foundation

enum APPError: Error {
    case urlConfigurationError(String)
    case serverError(Int)
    case decodingError(Error)
}
