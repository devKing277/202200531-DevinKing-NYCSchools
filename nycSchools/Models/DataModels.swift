//
//  DataModels.swift
//  nycSchools
//
//  Created by  on 5/26/22.
//

import Foundation

/// Conforms objects to a type that can be decoded. Generic methods can use this object to identify associated data types.
protocol DataIdentifiable: Codable {}

/// Conforms objects to a type that can provide information about a high school including school name and boro.
protocol SchoolIdentifiable: DataIdentifiable {
    var dbn: String { get set }
    var boro: String { get set }
    var name: String { get set }
    var location: String { get set }
    var numberOfStudents: String { get set }
}

/// Conforms objects to a type that can provide information about a high school's SAT scores including reading, writing and math scores.
protocol SATScoreIdentifiable: DataIdentifiable {
    var dbn: String { get set }
    var schoolName: String { get set }
    var numOfSatTestTakers: String? { get set }
    var satCriticalReadingAvgScore: String? { get set }
    var satMathAvgScore: String? { get set }
    var satWritingAvgScore: String? { get set }
}

/// A model to describe schools and their most recent SAT scores.
/// Schools are represents as ``SchoolInformationModel`` and scores are represented as ``SATScoreModel``
struct SchoolAndScoresDataModel: Identifiable, DataIdentifiable {
    
    var id: String { return school.name }
    var school: SchoolInformationModel
    var scores: SATScoreModel?
    
    static func == (lhs: SchoolAndScoresDataModel, rhs: SchoolAndScoresDataModel) -> Bool {
        lhs.id == rhs.id
    }
}

/// A model that defines the data from a successful API response containing a list of high schools in NYC.
class SchoolInformationModel: NSObject, SchoolIdentifiable {
    var dbn: String
    var boro: String
    var name: String
    var location: String
    var numberOfStudents: String
    
    private enum CodingKeys: String, CodingKey {
        case dbn
        case boro
        case name = "school_name"
        case location
        case numberOfStudents = "total_students"
    }
}

/// A model that defines the data from a successful API response containing the recent SAT scores for high schools in NYC.
struct SATScoreModel: SATScoreIdentifiable {
    var dbn: String
    var schoolName: String
    var numOfSatTestTakers: String?
    var satCriticalReadingAvgScore: String?
    var satMathAvgScore: String?
    var satWritingAvgScore: String?
    
    private enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}
