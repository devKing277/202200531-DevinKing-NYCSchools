//
//  SchoolAndScores.swift
//  nycSchools
//
//  Created by  on 5/28/22.
//

import Foundation

struct SchoolAndScores {
    typealias SchoolName = String
    var data: [SchoolName: SchoolAndScoresDataModel] = [:]
    
    init(data: [SchoolName: SchoolAndScoresDataModel]?) {
        self.data = data ?? [:]
    }
}
