//
//  APIFetchService.swift
//  nycSchools
//
//  Created by  on 5/26/22.
//

import Foundation

/// The API endpoint components of different data resource types.
enum APIResourceTypeURLComponents: String {
    
    case schools = "/s3k6-pzi2.json"
    case satScores = "/f9bf-2cp4.json"
}

/// A lightweight API fetch service that can return data resources including ``SchoolInformationModel`` and ``SATScoreModel``
enum APIFetchService {
    
    case schools
    case satScores
    case none
    
    /// The base url string to the API
    private var baseURL: String { return "https://data.cityofnewyork.us/resource" }
    
    /// A url string composed of a baseURL and data resource type component ``APIResourceTypeURLComponents``.
    private var path: String {
        var endpoint: String
        switch self {
        case .schools: endpoint = APIResourceTypeURLComponents.schools.rawValue
        case .satScores: endpoint = APIResourceTypeURLComponents.satScores.rawValue
        case .none: endpoint = ""
        }
        return baseURL + endpoint
    }
    
    /// An optional URL to the API
    private var url: URL? {
        guard let url = URL(string: path) else {
            assertionFailure("The URL is invalid because path: \(path) is not valid")
            return nil
        }
        return url
    }
        
    /// Asynchronously fetches data from a server based on the input type chosen at the call site.
    /// - Parameter type: The type to decode
    /// - Returns: An array of ``SchoolInformationModel`` or ``SATScoreModel`` .
    func fetchData<T: DataIdentifiable>(decodableType: [T].Type) async throws -> [T] {
        
        // 1.
        // Generate a valid URL based on type, otherwise return nil which
        // causes this guard statement to fail and return an empty array.
        guard let url: URL = {
            switch self {
            case .schools: return APIFetchService.schools.url
            case .satScores: return APIFetchService.satScores.url
            case .none: return nil
            }
        }() else {
            assertionFailure("bad url configuration")
            return []
        }
        
        // 2.
        /// Fetch data from the server
        let (data, response) = try await URLSession.shared.data(from: url)
        
        // 3. 
        /// If data isn't available then return an ``APPError`` otherwise, decode the data and return it.
        if let response = response as? HTTPURLResponse {
            if response.statusCode != 200 {
                throw APPError.serverError(response.statusCode)
            } else {
                /// Decode the data otherwise return an ``APPError``
                do {
                    let decodedSATScores = try JSONDecoder().decode (decodableType, from: data)
                    return decodedSATScores
                } catch {
                    throw APPError.decodingError(error)
                }
            }
        }
        
        return []
    }
}
