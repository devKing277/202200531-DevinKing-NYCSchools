//
//  RootNavigation.swift
//  nycSchools
//
//  Created by  on 5/29/22.
//

import Foundation
import UIKit

/// The root navigation of this project.
class RootNavigation: UINavigationController {
    init(rootVC: UIViewController) {
        super.init(rootViewController: rootVC)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
