//
//  SchoolCell+.swift
//  nycSchools
//
//  Created by  on 5/29/22.
//

import Foundation

extension SchoolCell {
    /// Gets the full borough name based on a character like "M" for Manhattan and "K" for Brooklyn.
    func getFullBoroughName(character: String) -> String {
        var boro = ""
        switch character {
        case "M": boro = "Manhattan"
        case "K": boro = "Brooklyn"
        case "X": boro = "Bronx"
        case "Q": boro = "Queens"
        case "S": boro = "Staten Island"
        default: boro = ""
        }
        return boro
    }
}
