//
//  SchoolCellBasic.swift
//  nycSchools
//
//  Created by  on 5/28/22.
//

import Foundation
import UIKit

class SchoolCell: UITableViewCell {
    
    /// The user action delegate that communicates back to the viewModel
    weak var delegate: SchoolCellUserActionDelegate?
        
    private let boroughLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = .preferredFont(forTextStyle: .caption1)
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 1
        return label
    }()
    
    private let schoolNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .preferredFont(forTextStyle: .headline)
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 2
        return label
    }()
    
    private let mainVStack: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillProportionally
        stackView.axis = .vertical
        stackView.spacing = 5
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(mainVStack)
        
        configureMainStackViewConstraints()
        addArrangedViewsToMainVStack(views: [schoolNameLabel, boroughLabel])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addArrangedViewsToMainVStack(views: [UIView]) {
        views.forEach({ view in
            mainVStack.addArrangedSubview(view)
        })
    }
    
    func setupCell(atIndex: Int, school: SchoolInformationModel?) {
        guard let school = school else { return }
        boroughLabel.text = getFullBoroughName(character: school.boro)
        schoolNameLabel.text = school.name
    }
    
    func configureMainStackViewConstraints() {
        mainVStack.anchor(
            top: topAnchor, paddingTop: 8,
            bottom: nil, paddingBottom: 0,
            left: leadingAnchor, paddingLeft: 20,
            right: trailingAnchor, paddingRight: -20,
            width: nil,
            height: nil
        )
    }
    
    @objc func userTappedHighSchool(with school: SchoolInformationModel) {
        delegate?.userTappedHighSchoolCell(school: school)
    }
}
