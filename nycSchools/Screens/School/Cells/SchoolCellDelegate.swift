//
//  SchoolCellDelegate.swift
//  nycSchools
//
//  Created by  on 5/29/22.
//

import Foundation

protocol SchoolCellUserActionDelegate: AnyObject {
    func userTappedHighSchoolCell(school: SchoolInformationModel)
}
