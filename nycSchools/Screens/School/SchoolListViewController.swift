//
//  RootViewController.swift
//  nycSchools
//
//  Created by  on 5/26/22.
//

import UIKit

class SchoolListViewController: UIViewController {
    
    // MARK: - Dependencies
    let viewModel: SchoolListViewModel
    let mainTableView: UITableView
    
    // MARK: - Initializers
    init(viewModel: SchoolListViewModel?, tableView: UITableView?) {
        
        let defaultViewModel = SchoolListViewModel(schoolData: nil, delegate: nil)
        let defaultUITable = UITableView()
        
        self.viewModel = viewModel ?? defaultViewModel
        self.mainTableView = tableView ?? defaultUITable
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        view.addSubview(self.mainTableView)
        
        configureViewStyle()
        configureTableViewConstraints()
        configureViewModel()
        configureDelegates()
    }
}

// MARK: - View Configurations
extension SchoolListViewController {
    
    /// Configures the styling of this view
    func configureViewStyle() {
        title = "SAT Scores"
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    /// Configures the main table view constraints
    func configureTableViewConstraints() {
        mainTableView.pinTo(view: view)
    }
    
    /// Configures all delegates related to this view
    func configureDelegates() {
        mainTableView.delegate = self
        mainTableView.dataSource = self
        
        viewModel.delegate = self
    }
    
    // TODO: - Load from core data while awaiting for new data
    /// Loads the data to this view
    func configureViewModel() {
        Task { await viewModel.loadSchoolDataToViewModel() }
    }
    
}
// MARK: - Datasource and Delegate

extension SchoolListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        // The default height of each row
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // The number of schools with available data
        return self.viewModel.schoolData.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // A school object at the index of the current row
        let school = self.viewModel.orderedListOfSchools[indexPath.row]
        
        // Cell registration with the tableview
        tableView.register(SchoolCell.self, forCellReuseIdentifier: "SchoolCell")
        
        // Dequeue a cell and configure it
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolCell") as! SchoolCell
        cell.setupCell(atIndex: indexPath.row, school: school)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
        // The name of the school at the selected row index
        let schoolName = self.viewModel.orderedListOfSchools[indexPath.row].name
        
        // The selected school data set is passed to the delegate
        if let schoolData = self.viewModel.schoolData.data[schoolName] {
            userTappedHighSchoolCell(data: schoolData)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func userTappedHighSchoolCell(data: SchoolAndScoresDataModel) {
        DispatchQueue.main.async { [weak self] in
            let scoresViewModel = ScoresViewModel(scoreData: data)
            let scoresVC = ScoresViewController(viewModel: scoresViewModel, scoresView: nil)
            scoresVC.modalPresentationStyle = .automatic
            self?.present(scoresVC, animated: true)
        }
    }
}
// MARK: - View Methods
extension SchoolListViewController: SchoolListViewDelegate {
    
    /// Notify the tableview that new data is available, and, as long as there are no uncommitted updates, ask it to reload data.
    func dataDidUpdate() {
        DispatchQueue.main.async { [weak self] in
            if self?.mainTableView.hasUncommittedUpdates == false {
                self?.mainTableView.reloadData()
            }
        }
    }
    
}




