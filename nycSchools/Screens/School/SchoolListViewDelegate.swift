//
//  SchoolListViewDelegate.swift
//  nycSchools
//
//  Created by  on 5/27/22.
//

import Foundation

protocol SchoolListViewDelegate: AnyObject {
    func dataDidUpdate()
}
