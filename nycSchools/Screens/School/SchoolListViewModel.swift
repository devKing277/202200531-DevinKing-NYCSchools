//
//  SchoolListViewModel.swift
//  nycSchools
//
//  Created by  on 5/26/22.
//

import Foundation

// MARK: - View Model

class SchoolListViewModel: NSObject {
    
    /// A delegate used to communicate back to the view controller.
    weak var delegate: SchoolListViewDelegate?
    
    /**
     A dictionary of school data where the key is the school name and value is all available data about the school.
     
     **didSet triggers**
     1. Calls the delegate to reload its data.
     */
    var schoolData: SchoolAndScores {
        didSet(school) {
            delegate?.dataDidUpdate()
        }
    }
    
    /// An array of  school model objects that gets its data from the schoolData object on this view model.
    var orderedListOfSchools: [SchoolInformationModel] = []
    
    /// A required initializer for this view model to function.
    init(schoolData: SchoolAndScores?, delegate: SchoolListViewDelegate?) {
        
        let defaultSchoolAndScores = SchoolAndScores(data: nil)
        self.schoolData = schoolData ?? defaultSchoolAndScores
        self.delegate = delegate
    }
    
}

// MARK: - User Actions

extension SchoolListViewModel {
    /**
     Loads school and score data asynchronously from the ``APIFetchService`` and assigns the response to a
     local property on the view model as a dictionary where the key is a ``SchoolInformationModel`` and the
     value is an ``SATScoreModel`` containing all available SAT scores for that school.
     
     **Operational Steps**
     1. Await and assign fetched data responses to local properties schools and SAT scores.
     2. Sort the SAT scores.
     3. Create a dictionary by iterating through the schools array and assigning the school and score data as key and value, respectively.
     1. At each iteration, use the school's dbn to find the first SAT score data set that matches the current school iteration.
     2. Create a ``SchoolAndScoresDataModel`` object using the current school iteration, along with its associated scores.
     3. Add the ``SchoolAndScoresDataModel`` object to the viewModels schoolData dictionary.
     */
    func loadSchoolDataToViewModel() async {
        Task {
            // 1.
            var scores = try? await APIFetchService.satScores.fetchData(decodableType: [SATScoreModel].self)
            let schools = try? await APIFetchService.schools.fetchData(decodableType: [SchoolInformationModel].self)
            // 2.
            scores = scores?.sorted(by: { $0.dbn < $1.dbn })
            // 3.
            for school in schools ?? [] {
                // 1.
                if let score = scores?.first(where: {
                    school.dbn.isEmpty == false &&
                    school.dbn == $0.dbn
                }) {
                    // 2.
                    let collectiveData = SchoolAndScoresDataModel(school: school, scores: score)
                    // 3.
                    schoolData.data[school.name] = collectiveData
                    // 4.
                    orderedListOfSchools.append(school)
                }
            }
        }
    }
}
