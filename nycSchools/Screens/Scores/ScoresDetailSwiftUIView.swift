//
//  ScoresDetailSwiftUIView.swift
//  nycSchools
//
//  Created by  on 5/29/22.
//

import SwiftUI

struct ScoresDetailSwiftUIView: View {
    
    // MARK: - Dependencies
    var viewModel: ScoresDetailSwiftUIViewModel
    
    // MARK: - Local view properties
    var defaultOffset: CGFloat = 20
    
    var body: some View {
        VStack(alignment: .leading, spacing: 25) {
            schoolNameSection()
                .padding(.horizontal, defaultOffset)
            VStack(alignment: .center, spacing: 25) {
                satSection()
                schoolFacts()
            }
        }
        .offset(x: 0, y: defaultOffset)
    }
    
    /// The name, address and LAT/LONG of this school
    func schoolNameSection() -> some View {
        VStack(alignment: .leading, spacing: 5) {
            Text(viewModel.name)
                .font(.title3.weight(.bold))
                .multilineTextAlignment(.leading)
            Text(viewModel.address)
                .font(.caption.weight(.regular))
                .foregroundColor(.black)
        }
    }
    
    /// The SAT section of this view
    func satSection() -> some View {
        VStack(alignment: .leading, spacing: 10) {
            VStack(alignment: .leading, spacing: 3) {
                Text("SAT Scores")
                    .font(.headline.weight(.bold))
                    .foregroundColor(.black)
                
            }
            .offset(x: defaultOffset, y: 0)
            
            ScrollView(.horizontal, showsIndicators: false, content: {
                HStack(spacing: 10) {
                    scoreCard(name: "reading", score: viewModel.readingScore)
                    scoreCard(name: "writing", score: viewModel.writingScore)
                    scoreCard(name: "math", score: viewModel.mathScore)
                }
                .offset(x: defaultOffset, y: 0)
            })
        }
    }
    
    /// A list of facts related the school
    func schoolFacts() -> some View {
        ScrollView(.vertical) {
            VStack(alignment: .leading, spacing: 0) {
                List(Array(viewModel.schoolFacts.keys), id: \.self) { factName in
                    HStack {
                        Text(factName)
                        Spacer()
                        Text(viewModel.schoolFacts[factName] ?? "")
                    }
                    .font(.system(.callout, design: .default))
                }
                .listStyle(.plain)
                .frame(height: CGFloat(viewModel.schoolFacts.count) * 50)
            }
        }
    }
    
    /// A score card representing the average score of test takers in a given category.
    func scoreCard(name: String, score: String) -> some View {
        Button(action: {
            print("")
        }, label: {
            VStack(alignment: .center, spacing: 5, content: {
                Rectangle()
                    .cornerRadius(4, corners: [.topLeft, .topRight])
                    .foregroundColor(.blue)
                    .frame(height: 20)
                    .overlay(
                        Text(name)
                            .foregroundColor(.white)
                            .font(.system(.caption2, design: .rounded).weight(.bold))
                    )
                Text(score)
                    .font(.system(.footnote, design: .default).weight(.semibold))
                    .padding(.bottom, 5)
            })
            .foregroundColor(.black)
            .textCase(.uppercase)
            .frame(width: 80)
            .background(
                RoundedRectangle(cornerRadius: 4)
                    .strokeBorder(.blue, style: .init(lineWidth: 0.40))
                    .foregroundColor(.blue)
                    .overlay(Color.white.cornerRadius(4).padding(1))
                
                
            )
        })
    }
}

struct ScoresDetailSwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        Color.white
            .sheet(isPresented: .constant(true), content: {
                ScoresDetailSwiftUIView(viewModel:
                                            ScoresDetailSwiftUIViewModel(
                                                name: "High School Economics and Finance",
                                                address: "100 Trinity Place, Manhattan NY",
                                                readingScore: "323",
                                                writingScore: "214",
                                                mathScore: "458",
                                                schoolFacts: [
                                                    "Number of students" : "435"
                                                ])
                )
            })
    }
}
