//
//  ScoresDetailSwiftUIViewModel.swift
//  nycSchools
//
//  Created by  on 5/29/22.
//

import Foundation

struct ScoresDetailSwiftUIViewModel {
    
    var name: String
    var address: String
    var readingScore: String
    var writingScore: String
    var mathScore: String
    var schoolFacts: [String:String]
}

extension ScoresDetailSwiftUIViewModel {
    init(from dataSet: SchoolAndScoresDataModel) {
        self.name = dataSet.school.name
        self.address = dataSet.school.location
        self.readingScore = dataSet.scores?.satCriticalReadingAvgScore ?? ""
        self.writingScore = dataSet.scores?.satWritingAvgScore ?? ""
        self.mathScore = dataSet.scores?.satMathAvgScore ?? ""
        self.schoolFacts = [
            "Number of test takers": dataSet.scores?.numOfSatTestTakers ?? ""
        ]
    }
}
