//
//  SchoolDetailViewController.swift
//  nycSchools
//
//  Created by  on 5/28/22.
//

import Foundation
import UIKit

class ScoresViewController: UIViewController {
    
    // MARK: - Dependencies
    let viewModel: ScoresViewModel
    var scoresView: ScoresDetailSwiftUIView?
    
    // MARK: - Initializers
    init(viewModel: ScoresViewModel, scoresView: ScoresDetailSwiftUIView?) {
        
        self.viewModel = viewModel
        self.scoresView = scoresView
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        let scoresViewModel = ScoresDetailSwiftUIViewModel(from: viewModel.scoreData)
        addSubSwiftUIView(ScoresDetailSwiftUIView(viewModel: scoresViewModel), to: view)
    }
    
}
