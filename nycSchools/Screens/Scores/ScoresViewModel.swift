//
//  ScoresViewModel.swift
//  nycSchools
//
//  Created by  on 5/29/22.
//

import Foundation

class ScoresViewModel: NSObject {
    
    /// A school and scores data model
    var scoreData: SchoolAndScoresDataModel
    
    init(scoreData: SchoolAndScoresDataModel) {
        self.scoreData = scoreData
    }
}
